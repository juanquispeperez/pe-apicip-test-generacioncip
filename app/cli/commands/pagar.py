import click
from cli.cli import pass_context
from cli.proceso import core_bcp
from cli.utils import constants

@click.command()
@click.option('--env', default='dev',  help='Ambiente de despliegue (dev/pre/prod).')
@click.option('--input_file', default='PE-BCP.xlsx',  help='Archivo datos de entrada en formato Excel.')
@click.option('--storage', default='local',  help='Almacenamiento de los archivos inputs/outputs (local/s3).')
@pass_context
def command(ctx, **kwargs):
    """Test de pago del API BanksIntXml"""
    click.echo("=== Iniciando proceso de pago ===")
    core_bcp.core(constants.PAGAR, constants.TEST_OPERATION_PAGAR, ctx, **kwargs)
    click.echo("=== Finalizando proceso de pago ===")