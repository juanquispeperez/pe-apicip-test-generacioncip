import json
import requests
from suds.client import Client
from cli.utils import constants

def validate_status_code(response):
    if response.status_code == constants.STATUS_CODE_503:
        raise Exception('Status Code: {}'.format(response.status_code))
        
class PaymentApi(object):
    def __init__(self, base_url):
        self.headers = { 'cache-control': 'no-cache'}
        self.base_url = base_url

    def set_headers(self, headers):
        if headers is not None:
            if not 'cache-control' in sorted(headers):
                headers.update(self.headers)
            self.headers = headers

    def get_des_cod_serv(bank,codws,codesperado,opcion):
        mensaje = ""
        if bank==constants.BANCO_BCP:
            mensaje = "se esperaba otro código de respuesta"
            if codesperado == False:
                return mensaje
            
            if opcion==constants.CONSULTAR:
                if codws=="001":
                    mensaje="Consulta Realizada con éxito"
                elif codws=="002":
                    mensaje="Orden de pago con estado Anulado/Cancelado/Eliminado ó Expirado"
                elif codws=="003":                
                    mensaje="Cliente no encontrado , Hubo un error en la aplicación , Codigo no presenta valor"

            if opcion==constants.PAGAR:
                if codws=="001":
                    mensaje="Transaccion exitosa"
                elif codws=="004":
                    mensaje="Codigo de Agencia Bancaria sin valor / Cancelacion de Orden de Pago incorrecto / Numero de Orden de Pago vacio u/o cancelacion de la Orden de Pago incorrecto / Enviar de Correo de Cancelacion de Orden de Pago incorrecto / Registro de Servicio de Notificacion de Url Ok, incorrecta / Hubo un error en la aplicación"
        
            if opcion==constants.EXTORNAR:
                if codws=="001":
                    mensaje="Transaccion exitosa"
                elif codws=="005":
                    mensaje="Validación para la anulación del CIP falló - No se encontro un código de Servicio asociado al error / Validación para la anulación del CIP falló - Se excedió el tiempo máximo de extorno / Validación para la anulación del CIP falló - La fecha de extorno no coincide con la fecha de Pago / Validación para la anulación del CIP falló - La Agencia bancaria no es la misma donde se cancelo / Validación para la anulación del CIP falló - La Orden de Pago no se encuentra cancelada / Validación para la anulación del CIP falló - No se encontró la orden de Pago / Extorno no realizado - No se pudo realizar el movimiento / Registro de Servicio de Notificacion URL Error, incorrecto / Hubo un error en la aplicación"
        return mensaje

    def get_cips(self, bank, payload,opcion):
        r = requests.post(self.base_url, headers=self.headers, data=json.dumps(payload))
        return r