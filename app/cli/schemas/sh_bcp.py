from cli.utils.etiqueta import lenetiqueta
from cli.utils import constants

class ObtenerTramas(object):
        def __init__(self,bank,opcion):
                self.bank = bank
                self.opcion = opcion

        def obtener_idioma(self):
                idioma = ""
                if self.bank=="PACIFICO":
                        idioma="ES-PE"
                return idioma

        def obtener_trama_default(self,data):
                trama = {}
                if self.bank=="BCP":
                        cip = str(data[constants.REQ_CIP]).split(".")[0]            
                        if self.opcion==constants.CONSULTAR:
                                trama = {
                                "orderId": cip,
                                "amount":data[constants.REQ_MONTO],
                                "currencyId":1,
                                "validatorFields":"amount,currency"
                                }
                        if self.opcion==constants.PAGAR:
                                trama = {
                                "orderId": cip,
                                "currencyId": 1,
                                "movementTypeId": 34,
                                "paymentMethodId":2,
                                "amount": data[constants.REQ_MONTO],
                                "sourceId": 1,
                                "operationNumber": "1234567",
                                "originCancelationId": 1,
                                "channelCode": data[constants.REQ_CANAL],
                                "bankCode": "02",
                                "bankId": 1,
                                "serviceCode":"0004",
                                "validatorFields":"serviceCode,bankCurrency,currencyId,amount"                               
                                }
                        if self.opcion==constants.EXTORNAR:
                                trama = {
                                "orderId": cip,
                                "operationNumber": "1234567",
                                "paymentOperationNumber":"1234567",
                                "originCancelationId": 1,
                                "amount": "-"+ data[constants.REQ_MONTO],
                                "currencyId": 1,
                                "validatorFields":"0004",                                
                                "serviceCode":"serviceCode,bankCurrency,currencyId,amount"
                                }
                return trama