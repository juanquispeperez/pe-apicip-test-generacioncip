from marshmallow import Schema, fields

class PaymentRequestSchemaC(Schema):
    orderId = fields.Str(required=True)
    amount = fields.Str(required=True)
    currencyId = fields.Str(required=True)
    validatorFields = fields.Str(required=True)
    
class PaymentRequestSchemaP(Schema):    
    orderId = fields.Str(required=True)
    currencyId = fields.Str(required=True)
    movementTypeId = fields.Str(required=True)
    paymentMethodId = fields.Str(required=True)
    amount = fields.Str(required=True)
    sourceId = fields.Str(required=False)
    operationNumber = fields.Str(required=True)
    originCancelationId = fields.Str(required=True)
    channelCode = fields.Str(required=True)
    bankCode = fields.Str(required=True)
    bankId = fields.Str(required=True)
    serviceCode = fields.Str(required=True)
    validatorFields = fields.Str(required=True)

class PaymentRequestSchemaE(Schema):
    orderId = fields.Str(required=True)
    operationNumber = fields.Str(required=True)
    originCancelationId = fields.Str(required=True)
    paymentOperationNumber = fields.Str(required=False)
    amount = fields.Str(required=False)
    currencyId = fields.Str(required=False)
    serviceCode = fields.Str(required=True)
    validatorFields = fields.Str(required=True)