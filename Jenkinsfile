def fnSteps = evaluate readTrusted("deploy/steps.groovy")

pipeline {
    agent any
    stages {
        stage('Set Config') {
            steps { 
                script { 
                    config = fnSteps.configs("${JENKINS_ENV}", params.RUNTEST, params.BANK)
                    withEnv(config) { fnSteps.call("Desplegar") }
                }
            }
        }
        stage('Login ecr aws') {
            steps { script { fnSteps.login_aws_ecr(config) }}
        }
        stage('Create repository ecr') {
            steps { script { fnSteps.create_repository(config) }}
        }
        stage('Deploy') {
            when { expression { return params.Desplegar }}
            steps { 
                script { 
                    fnSteps.build_latest(config)
                    fnSteps.push_aws_ecr(config)
                    fnSteps.stack_deploy(config)
                }
            }
        }
        stage('Delete images of ecr') {
            steps { script { fnSteps.batch_delete_image_aws_ecr(config) }}
        }
        stage('Run test') {
            steps { script { fnSteps.run_task(config) }}
        }
    }
    post { 
        always { 
            echo 'Execution finished.'
            cleanWs()
        }
        success {
            echo 'Success!! :)'
            script { 
                withEnv(config) {
                    fnSteps.call("SUCCESS")
                }
            }
        }
        failure {
            echo 'Failure!! :)'
            script { 
                withEnv(config) {
                    fnSteps.call("FAILURE")
                }
            }
        }
    }
    parameters {
        booleanParam(
            name: 'Desplegar',
            defaultValue: true, 
            description: 'Se despliega la automatización')
        choice(
            name: 'RUNTEST',
            choices:["consultar","pagar","extornar","operacion"],
            description: 'Ejecuta el test selecionado')
        choice(
            name: 'BANK',
            choices:["BCP"],
            description: 'Banco que ejecutra el test')
    }
}
