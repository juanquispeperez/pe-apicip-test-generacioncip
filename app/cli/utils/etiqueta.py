from cli.utils import constants
from cli.utils.testcase import TestCases

class Etiquetas(object):
    def __init__(self,bank):
        self.bank = bank

    def valida_etiqueta(self,data,trama,hoja):
        etiqueta = data[constants.REQ_ETIQUETA]
        tramaint = trama
        try:
            for key,val in trama.items():
                if key == etiqueta:
                    metodo = data[constants.REQ_TIPOPRUEBA]
                    if metodo != "satisfactorio":
                        longitud = lenetiqueta(self.bank,key)
                        test = TestCases(longitud,hoja)
                        if metodo == "eliminar_etiqueta":
                            tramaint.pop(key)
                        else:
                                for ty in type(test).mro():
                                    if metodo in ty.__dict__:
                                        ClassMethod = getattr(test,metodo)                         
                                        tramaint[key] = ClassMethod()
                                        break
                    break
        except Exception as e:
            print("valida_etiqueta(), Se detectó el error siguiente:")
            print(e)
        return tramaint

def lenetiqueta(banco,etiqueta):
        LONGITUD = 0        
        if banco == constants.BANCO_BCP:
            if etiqueta == constants.E_ORDERID:
                LONGITUD = 12
            elif etiqueta == constants.E_CURRENCYID:
                LONGITUD = 10                     
            elif etiqueta == constants.E_MOVEMENTTYPEID:
                LONGITUD = 10  
            elif etiqueta == constants.E_PAYMENTMETHODID:
                LONGITUD = 10     
            elif etiqueta == constants.E_AMOUNT:
                LONGITUD = 10
            elif etiqueta == constants.E_PAYMENTOPERATIONNUMBER:
                LONGITUD = 10
            elif etiqueta == constants.E_SOURCEID:
                LONGITUD = 12
            elif etiqueta == constants.E_OPERATIONNUMBER:
                LONGITUD = 10
            elif etiqueta == constants.E_PAYMENTSOURCETYPEID:
                LONGITUD = 10
            elif etiqueta == constants.E_CHAANNELCODE:
                LONGITUD = 10
            elif etiqueta == constants.E_BANKCODE:
                LONGITUD = 10
            elif etiqueta == constants.E_BANKID:
                LONGITUD = 10
            elif etiqueta == constants.E_SALEPOINTID:
                LONGITUD = 10
            elif etiqueta == constants.E_SALEPOINTTERMINALID:
                LONGITUD = 10
            elif etiqueta == constants.E_AUDITNUMBER:
                LONGITUD = 100
            elif etiqueta == constants.E_VALIDATORFIELDS:
                LONGITUD = 100
            elif etiqueta == constants.E_SERVICECODE:
                LONGITUD = 10
            elif etiqueta == constants.E_ORIGINCANCELATIONID:
                LONGITUD = 10
        return LONGITUD