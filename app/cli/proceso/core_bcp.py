import click
import json
import uuid
from cli.api.servicios import PaymentApi
from cli.api import servicios
from cli.utils import fechas
from datetime import datetime
from cli.schemas import sh_bcp
from cli.schemas.payment import PaymentRequestSchemaC
from cli.schemas.payment import PaymentRequestSchemaP
from cli.schemas.payment import PaymentRequestSchemaE
from cli.utils import etiqueta
from cli.utils.config import get_endpoint_payments
from cli.utils.file import validate_exist_file
from cli.utils import xls
from cli.utils.file import get_file_from_storage
from cli.utils.file import put_file_to_storage
from cli.utils import constants
from cli.utils.report import generate_report
from cli.utils.report import get_report_name
from cli.utils.report import save_report_to_file
from cli.utils.report import get_output_dir

def prepare_payload(data,ope):
    schema = None
    payload = None

    if ope == constants.CONSULTAR:
        schema = PaymentRequestSchemaC()
        payload = dict(
        orderId = str(data[constants.E_ORDERID]),
        amount = str(data[constants.E_AMOUNT]),
        currencyId = str(data[constants.E_CURRENCYID]),
        validatorFields = str(data[constants.E_VALIDATORFIELDS])
        )

    if ope == constants.PAGAR:
        schema = PaymentRequestSchemaP()
        payload = dict(
        orderId = str(data[constants.E_ORDERID]),
        currencyId = str(data[constants.E_CURRENCYID]),
        movementTypeId = str(data[constants.E_MOVEMENTTYPEID]),
        paymentMethodId = str(data[constants.E_PAYMENTMETHODID]),
        amount = str(data[constants.E_AMOUNT]),
        sourceId = str(data[constants.E_SOURCEID]),
        operationNumber = str(data[constants.E_OPERATIONNUMBER]),
        originCancelationId = str(data[constants.E_ORIGINCANCELATIONID]),
        channelCode = str(data[constants.E_CHAANNELCODE]),
        bankCode = str(data[constants.E_BANKCODE]),
        bankId = str(data[constants.E_BANKID]),
        serviceCode = str(data[constants.E_SERVICECODE]),
        validatorFields = str(data[constants.E_VALIDATORFIELDS])
        )

    if ope == constants.EXTORNAR:
        schema = PaymentRequestSchemaE()
        payload = dict(
        orderId = str(data[constants.E_ORDERID]),
        operationNumber = str(data[constants.E_OPERATIONNUMBER]),
        originCancelationId = str(data[constants.E_ORIGINCANCELATIONID]),
        paymentOperationNumber = str(data[constants.E_PAYMENTOPERATIONNUMBER]),
        amount = str(data[constants.E_AMOUNT]),
        currencyId = str(data[constants.E_CURRENCYID]),
        serviceCode = str(data[constants.E_SERVICECODE]),
        validatorFields = str(data[constants.E_VALIDATORFIELDS])
        )
    return schema.dump(payload)

def validate_payload(payload):
    if payload != None:
        if len(payload.errors) != 0 :
            raise Exception(payload.errors)

def get_header_language(data):
    idioma = None
    if constants.HEADER_COLUMN_LANGUAGE != None:
        idioma = data[constants.HEADER_COLUMN_LANGUAGE]
    return idioma

def prepare_headers(language):
    headers = None
    if language != None:
        headers = { 
            'Content-type': 'application/json',
            'Origin':'test-Qa',
            'Accept-Language':language
        }
    return headers

def criterios_aceptacion(response_json,data,opcion):
    sin_errores = True
    mensaje = " "
    j_code = str(response_json['code'])
    if j_code != '100':
        codejson =  response_json['data'][0].get('code')        
        msjson =  response_json['data'][0].get('message')       
        fieldjson = response_json['data'][0].get('field')        

        codexls = None
        msjxls = None
        fieldxls = None

        if str(data[constants.CRA_CE_CODE]).strip() != "": codexls = data[constants.CRA_CE_CODE]
        if str(data[constants.CRA_CE_MESSAGE]).strip() != "": msjxls = data[constants.CRA_CE_MESSAGE]
        if str(data[constants.CRA_CE_FIELD]).strip() != "": fieldxls = data[constants.CRA_CE_FIELD]
        
        if codejson != codexls:
            mensaje = mensaje + " El campo 'code' devuelto por el servicio, no coincide con lo esperado"
            print(mensaje)
            print("code json:")
            print(codejson)
            print("code xls:")
            print(codexls)
            sin_errores = False

        if msjson != msjxls:
            mensaje = mensaje + "El campo 'message' devuelto por el servicio, no coincide con lo esperado"
            print(mensaje)
            print("message json: ")
            print(msjson)
            print("message xls: ")
            print(msjxls)
            sin_errores = False

        if fieldjson != fieldxls:
            mensaje = mensaje + "El campo 'field' devuelto por el servicio, no coincide con lo esperado"
            print(mensaje)
            print("field json: ")
            print(fieldjson)
            print("field xls: ")
            print(fieldxls)
            sin_errores = False
    else:
        if str(response_json['data']['orderId']) != data[constants.CRA_ORDERID] :
            mensaje = "El campo 'orderId' devuelto por el servicio, no coincide con lo esperado"
            print(mensaje)
            sin_errores = False        
        if opcion==constants.CONSULTAR:
            if "{0:.2f}".format(response_json['data']['totalAmount']) != data[constants.CRA_AMOUNT]:
                mensaje = "El campo 'totalAmount' devuelto por el servicio, no coincide con lo esperado"
                print(mensaje)
                sin_errores = False                
            if str(response_json['data']['orderStatus']) != data[constants.CRA_ORDERSTATUS]:
                mensaje = "El campo 'orderStatus' devuelto por el servicio, no coincide con lo esperado"
                print(mensaje)
                sin_errores = False
            if str(response_json['data']['serviceId']) != data[constants.CRA_SERVICEID]:
                mensaje = "El campo 'serviceId' devuelto por el servicio, no coincide con lo esperado"
                print(mensaje)
                sin_errores = False
            if str(response_json['data']['currency']) != data[constants.CRA_CURRENCY]:
                mensaje = "El campo 'currency' devuelto por el servicio, no coincide con lo esperado"
                print(mensaje)
                sin_errores = False
            if str(response_json['data']['orderNumber']) != data[constants.CRA_ORDERNUMBER]:
                mensaje = "El campo 'orderNumber' devuelto por el servicio, no coincide con lo esperado"
                print(mensaje)
                sin_errores = False
            if str(response_json['data']['createdUserId']) != data[constants.CRA_CREATIONUSERID]:
                mensaje = "El campo 'createdUserId' devuelto por el servicio, no coincide con lo esperado"
                print(mensaje)
                sin_errores = False
            massiveCharge_json = False if response_json['data']['massiveCharge']==0 else True           
            if str(massiveCharge_json) != data[constants.CRA_MASSIVECHARGE]:
                mensaje = "El campo 'massiveCharge' devuelto por el servicio, no coincide con lo esperado"
                print(mensaje)
                sin_errores = False        
        else:
            monto1 = float(response_json['data']['amount'])
            monto2 = float(data[constants.CRA_AMOUNT])

            if opcion==constants.EXTORNAR:
                monto2 = monto2 * -1

            if monto1 != monto2:
                mensaje = "El campo 'amount' devuelto por el servicio, no coincide con lo esperado"
                print(mensaje)
                sin_errores = False

            movementTypeId_xls = ""
            len_movementTypeId_xls = len(str(data[constants.CRA_MOVEMENTTYPEID]).split(","))            
            if len_movementTypeId_xls > 1:
                if opcion==constants.PAGAR:
                    movementTypeId_xls = str(data[constants.CRA_MOVEMENTTYPEID]).split(",")[0]
                else:
                    movementTypeId_xls = str(data[constants.CRA_MOVEMENTTYPEID]).split(",")[1]
            else:
                movementTypeId_xls = str(data[constants.CRA_MOVEMENTTYPEID])

            
            if str(response_json['data']['movementTypeId'])  != movementTypeId_xls:            
                mensaje = "El campo 'movementTypeId' devuelto por el servicio, no coincide con lo esperado"
                print(mensaje)
                sin_errores = False
            if str(response_json['data']['currencyId']) != data[constants.CRA_CURRENCYID]:            
                mensaje = "El campo 'currencyId' devuelto por el servicio, no coincide con lo esperado"
                print(mensaje)
                sin_errores = False            
            if str(response_json['data']['paymentMethodId']) != data[constants.CRA_PAYMENTMETHODID]:            
                mensaje = "El campo 'paymentMethodId' devuelto por el servicio, no coincide con lo esperado"
                print(mensaje)
                sin_errores = False
            
            if opcion==constants.PAGAR:
                if str(response_json['data']['operationNumber']) != data[constants.CRA_OPERATIONNUMBER]:            
                    mensaje = "El campo 'operationNumber' devuelto por el servicio, no coincide con lo esperado"
                    print(mensaje)
                    sin_errores = False
            else:
                if str(response_json['data']['operationNumber']) != data[constants.CRA_PAYMENTOPERATIONNUMBER]:            
                    mensaje = "El campo 'operationNumber' devuelto por el servicio, no coincide con lo esperado"
                    print(mensaje)
                    sin_errores = False

            if str(response_json['data']['paymentSourceTypeId']) != data[constants.CRA_ORIGINCANCELATIONID]:            
                mensaje = "El campo 'paymentSourceTypeId' devuelto por el servicio, no coincide con lo esperado"
                print(mensaje)
                sin_errores = False
            if str(response_json['data']['channelCode']) != data[constants.CRA_CHANNELCODE]:            
                mensaje = "El campo 'channelCode' devuelto por el servicio, no coincide con lo esperado"
                print(mensaje)
                sin_errores = False
            if str(response_json['data']['bankCode']) != data[constants.CRA_BANKCODE]:            
                mensaje = "El campo 'bankCode' devuelto por el servicio, no coincide con lo esperado"
                print(mensaje)
                sin_errores = False
            if str(response_json['data']['bankId']) != data[constants.CRA_BANKID]:            
                mensaje = "El campo 'bankId' devuelto por el servicio, no coincide con lo esperado"
                print(mensaje)
                sin_errores = False
    return sin_errores,mensaje

def process(sheet1,sheet2, bank, endpoint,opcion,tipo,ope):
    results = []
    api = PaymentApi(endpoint)
    
    OPERACION = constants.TEST_OPERATION + ' - ' + tipo
    TIPOPE = constants.OPERACION
    
    if ope == False:
        TIPOPE = opcion
        OPERACION = tipo

    cetiqueta = etiqueta.Etiquetas(bank)
    ctrama = sh_bcp.ObtenerTramas(bank,opcion)
    idioma = ctrama.obtener_idioma()    

    for row in range(4, sheet1.nrows):
        try:
            data = sheet1.row_values(row)
            if data[constants.REQ_TIPOOPE] != TIPOPE:
                continue

            headers = prepare_headers(get_header_language(data))
            api.set_headers(headers)
            trama = ctrama.obtener_trama_default(data)
            trama = cetiqueta.valida_etiqueta(data,trama,sheet2)

            payload = prepare_payload(trama,opcion)
            validate_payload(payload)            
            response = api.get_cips(bank,payload.data,opcion)
            
            servicios.validate_status_code(response)
            FILA = data[constants.REQ_NRO]
            response_json = response.json()
            
            j_code = response_json['code']
            j_message = response_json['message']
            
            estado,obs = criterios_aceptacion(response_json,data,opcion)

            payload_request = json.dumps(payload.data, indent=4)            
            payload_response = json.dumps(response_json, indent=4)
            casoprueba = data[constants.REQ_ETIQUETA]+" - "+data[constants.REQ_TIPOPRUEBA]            
            results.append({
                "orden": str(FILA),
                "limite":str(data[constants.REQ_LIM]),
                "idioma":idioma,
                "cip":str(data[constants.REQ_CIP]).split(".")[0],
                "modalidad": str(data[constants.REQ_MOD]).split(".")[0],
                "servicio": str(data[constants.REQ_CONVENIO]).split(".")[0],
                "moneda": str(data[constants.REQ_MONEDA]).split(".")[0].zfill(2),
                "monto":str(data[constants.REQ_MONTO]),
                "operacion":OPERACION,
                "etiqueta":str(data[constants.REQ_ETIQUETA]),
                "tipodeprueba":str(data[constants.REQ_TIPOPRUEBA]),
                "casoprueba":casoprueba,
                "canal":str(data[constants.REQ_CANAL]),
                "valoringresado": trama[data[constants.REQ_ETIQUETA]],
                "fecha": fechas.get_date_string(),
                "sinerror": estado,
                "observacion": obs,
                "codigoesp": str(data[constants.CRA_CODE]),
                "codigows": str(j_code),
                "descodigows": j_message,
                "request": {
                    "id": str(uuid.uuid4()),
                    "payload": payload_request
                },
                  "response":{
                    "id": str(uuid.uuid4()),
                    "payload": payload_response
                }
            })

            click.echo("Payload Request:")
            click.echo(payload_request)
            click.echo("Payload Response:")
            click.echo(payload_response)
            click.echo("============ SE EJECUTO EL TEST - " + tipo + " - FILA NRO. " + str(row+1) + " =============")
        except Exception as e:
            click.echo(e)
    return results

def get_bank(input_file):
    return input_file.split('.')[0].split('-')[1]

def prepare_data(operation,fechaini, fechafin, endpoint, results, duracion,tcasos,tincorrectos,tcorrectos):
    data = {
    'operation':operation,
    'fechaini': fechaini,
    'fechafin': fechafin,
    'endpoint': endpoint, 
    'results': results,
    'duracion': duracion,
    'tcasos': tcasos,
    'tincorrectos': tincorrectos,
    'tcorrectos': tcorrectos
    }
    return data

def core(opcion,tipo,ctx,**kwargs):
    try:
        click.echo("Storage Inputs/Outputs File: {}".format(kwargs['storage'])) 
        get_file_from_storage(
            ctx.config, 
            kwargs['env'], 
            kwargs['storage'], 
            kwargs['input_file']
        )
        if tipo != constants.TEST_OPERATION:
            endpoint = get_endpoint_payments(ctx.config, tipo, kwargs['env'])
            click.echo("Endpoint: {}".format(endpoint))
            
        input_file = kwargs['input_file']
        click.echo("Input File: {}".format(input_file))

        validate_exist_file(input_file)
        wb = xls.get_workbook(input_file)

        bank = constants.BANCO_BCP
        click.echo("Banco: {}".format(bank))

        initial_date = fechas.get_date()
        
        sh_operacion = xls.get_sheet_by_name(wb, constants.SHEET_INPUT)
        sh_cips = xls.get_sheet_by_name(wb, constants.SHEET_CIPS)

        results = None
        if tipo == constants.TEST_OPERATION:
            
            ENDPOINT_CO = get_endpoint_payments(ctx.config,constants.TEST_OPERATION_CONSULTAR, kwargs['env'])            
            ENDPOINT_CA = get_endpoint_payments(ctx.config,constants.TEST_OPERATION_PAGAR, kwargs['env'])
            ENDPOINT_EX = get_endpoint_payments(ctx.config,constants.TEST_OPERATION_EXTORNAR, kwargs['env'])
            endpoint = "Consultar&nbsp;:&nbsp;" + ENDPOINT_CO + "</br>" + "Pagar&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;" + ENDPOINT_CA + "</br>" + "Extornar&nbsp;&nbsp;&nbsp;:&nbsp;" + ENDPOINT_EX

            procesaConsulta = process(sh_operacion,sh_cips, bank,ENDPOINT_CO ,constants.CONSULTAR,constants.TEST_OPERATION_CONSULTAR,True)
            procesaPago = process(sh_operacion,sh_cips, bank,ENDPOINT_CA ,constants.PAGAR,constants.TEST_OPERATION_PAGAR,True)
            procesaExtorno = process(sh_operacion,sh_cips, bank,ENDPOINT_EX ,constants.EXTORNAR,constants.TEST_OPERATION_EXTORNAR,True)
            my_cool_list = procesaConsulta + procesaPago + procesaExtorno
            results = my_cool_list
        else:            
            results = process(sh_operacion,sh_cips, bank, endpoint,opcion,tipo,False)

        total_casos = len(results)
        total_incorrectos = 0
        total_correctos = 0

        for item in results:           
            if item["sinerror"] == False:
                total_incorrectos +=1

        total_correctos = total_casos - total_incorrectos
            
        click.echo("Generate html report with results of API")
        
        fechafin = fechas.get_date()
        duracion = fechafin - initial_date
        
        data = prepare_data(
            tipo,         
            initial_date.strftime("%d-%m-%Y %H:%M:%S"),
            initial_date.strftime("%d-%m-%Y %H:%M:%S"),
            endpoint, 
            results,
            duracion,
            total_casos,
            total_incorrectos,
            total_correctos
        )

        report_html = generate_report("report.html", data)
        report_name = ""
        if tipo==constants.TEST_OPERATION_CONSULTAR:
            report_name = get_report_name(f"rpt_consultar_{bank}")
        elif tipo==constants.TEST_OPERATION_PAGAR:
            report_name = get_report_name(f"rpt_pagar_{bank}")
        elif tipo==constants.TEST_OPERATION_EXTORNAR:
            report_name = get_report_name(f"rpt_extornar_{bank}")
        else:
            report_name = get_report_name(f"rpt_operacion_{bank}")

        click.echo("Storing report {} into {}".format(report_name, kwargs['storage']))
        output_dir = get_output_dir(kwargs['storage'])
        output_file = '{}/{}'.format(output_dir, report_name)
        save_report_to_file(output_file, report_html)
        put_file_to_storage(
            ctx.config, 
            kwargs['env'],
            kwargs['storage'], 
            output_file
        )

    except Exception as e:
        click.echo(str(e))