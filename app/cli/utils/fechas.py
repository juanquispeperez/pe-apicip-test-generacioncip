import pytz
from datetime import datetime

def get_date(region='America/Lima'):
	"""Returns the date according to the region sent."""
	fecha = None
	try:
		tz = pytz.timezone(region)
		fecha = datetime.now(tz)
	except pytz.exceptions.UnknownTimeZoneError:        
		print("invalid region.")
	return fecha

def get_date_string(region='America/Lima'):
	"""Returns the date in string according to the region sent."""			
	fecha = None
	try:
		tz = pytz.timezone(region)
		fecha = datetime.now(tz).strftime("%d-%m-%Y %H:%M:%S")
	except pytz.exceptions.UnknownTimeZoneError:
		print("invalid region.")
	return fecha